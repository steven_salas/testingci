# **Lantern tool** 🔦
Lantern tool es una herramienta de seguridad de contenedores que analiza el contenido de la rama master de tu repositorio de manera automatica al hacer algun cambio en la rama master.

Es un script de pruebas de seguridad diseñado para ejecutarse en el CI/CD pipeline de Gitlab

## **Alcance de análisis**
Lantern tool en su primera version, ejecuta las siguientes tareas:

* Calidad del codigo 
* Detección de secretos (testing) 
* Análisis estatico de vulnerabilidades con 2 motores de analisis (SAST)
 de forma simultanea

## **Uso**
Para utilizar **Lantern tool** se deben tener los siguientes requisitos:

* Una cuenta en Gitlab con permisos administrativos.
* Crear un nuevo proyecto publico en Gitlab.
* Crear 2 carpetas dentro del repositorio con los siguientes nombres:
    -  *anchore-reports*
    -  *reports*
* Copiar el contenido del archivo `.gitlab-ci.yaml` que se encuentra en este repositorio.
* Pegar el contenido de `.gitlab-ci.yaml` en un archivo vacío del mismo nombre en el repositorio de la cuenta Gitlab.

>⚠️ 
>**Nota:** Para el correcto funcionamiento de la herramienta, es **muy importante** que las carpetas y el archivo sean nombrados como se indica.  

* Copiar todo el contenido de la rama master del repositorio a analizar en el repositorio en Gitlab.

El repositorio debe quedar de la siguiente manera: 

![Estado inicial Repositorio](https://gitlab.com/steven_salas/testingci/-/raw/master/img/repo_listo.PNG)

Una vez cumplidos los requisitos solo queda iniciar el análisis. Se hace la siguiente manera: 

En la barra de menú ubicada en la parte izquierda vamos a seleccionar la opcion CI/CD > *Pipelines*

![Pipeline](https://gitlab.com/steven_salas/testingci/-/raw/master/img/cicd_-_pipelines.PNG)

Luego en la parte superior derecha hacer click en Run pipeline

![Iniciar pipeline](https://gitlab.com/steven_salas/testingci/-/raw/master/img/run_pipeline.PNG)

Al entrar en la tarea podremos ver las diferentes fases del analisis llevandose a cabo 

![Analisis en progreso](https://gitlab.com/steven_salas/testingci/-/raw/master/img/analisis_in_progress.PNG)

Cuando el analisis está listo se ve de la siguiente manera: 

![Analisis completado](https://gitlab.com/steven_salas/testingci/-/raw/master/img/analisis_listo.PNG)

>⚠️ 
>**_Nota:_**
>Si el estado del analisis se encuentra en pending, es porque se encuentra a a la espera de los recursos compartidos (shared runners) de Gitlab para ejecutar el analisis.

![Analisis pending](https://gitlab.com/steven_salas/testingci/-/raw/master/img/analisis_pending.PNG)

En caso de que se requiera utilizar un runner exclusivo para este proyecto, se debe adecuar la plataforma para este modelo. Podrás encontar mas informacion en el siguiente [enlace](https://docs.gitlab.com/runner/install/)  


## **Output (salidas)**
Las salidas de los análisis son generadas por la herramienta en formato Json y almacenan en el apartado de 'artifacts' dentro del pipeline de Gitlab. 

Para descargar las salidas, se debe ubicar el boton de descarga y descargar cada reporte. 

![Descarga de salida de analisis](https://gitlab.com/steven_salas/testingci/-/raw/master/img/artifacts.PNG)


## **Siguientes versiones**
Actualmente se está trabajando para integrar Lantern tool con Kubernetes, analisis dinámico y Dashboard.
